package noobbot;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public class Track {
    private final TrackPiece[] pieces;
    private final int[] trackPieceToComp;
    private final List<TrackComponent> trackComps;
    private final int[] lanes;
    private final double[][] lanePieceOffsets;

    public Track(TrackPiece[] pieces, int[] lanes) {
        this.pieces = pieces.clone();
        this.lanes = lanes.clone();
        trackPieceToComp = trackPieceToCompMap(pieces);
        trackComps = getTrackComps(lanes, pieces);
        lanePieceOffsets = getPieceOffsets();
    }

    public TrackPiece getPiece(int i) {
        return pieces[i];
    }

    public TrackComponent getComponent(int i) {
        return trackComps.get(i);
    }

    public int getNumComponents() {
        return trackComps.size();
    }

    public int getComponentIndexOfPiece(int i) {
        if (trackComps.isEmpty()) {
            return -1;
        }
        return trackPieceToComp[i];
    }

    public TrackComponent getComponentOfPiece(int i) {
        if (trackComps.isEmpty())
            return null;
        return trackComps.get(trackPieceToComp[i]);
    }

    public int getLaneOffset(int i) {
        return lanes[i];
    }

    public double getDistance(Car.Position p1, Car.Position p2) {
        if (p1 == null || p2 == null)
            return Double.POSITIVE_INFINITY;
        if (p1.lap == p2.lap && p1.pieceIndex == p2.pieceIndex)
            if (p1.laneFrom == p2.laneFrom && p1.laneTo == p2.laneTo)
                return p2.pieceDistance - p1.pieceDistance;
            else
                return Double.POSITIVE_INFINITY;
        if (p1.laneTo != p2.laneFrom)
            return Double.POSITIVE_INFINITY;
        double piece1Len = getPiece(p1.pieceIndex).getLaneLength(
                                getLaneOffset(p1.laneFrom), getLaneOffset(p1.laneTo));
        int p1Lap = p1.lap + (p1.pieceIndex + 1) / getNumPieces();
        int p1Index = (p1.pieceIndex + 1) % getNumPieces();
        return (piece1Len - p1.pieceDistance) + getDistanceOffset(p2) -
            getDistanceOffset(p1.laneTo, p1Lap, p1Index, 0);
    }

    /**
     * Gets the total distance to travel from the start of the 1st piece, to the
     * specified position by way of the given lane.
     */
    public double getDistanceOffset(int lane, int lap, int index, double pieceDistance) {
        return lap * lanePieceOffsets[lane][pieces.length] +
                    lanePieceOffsets[lane][index] +
                    pieceDistance;
    }

    public double getDistanceOffset(Car.Position p) {
        if (p == null)
            return 0;
        // TODO: When p is partway through a lane change the pieceDistance is not right.
        return getDistanceOffset(p.laneFrom, p.lap, p.pieceIndex, p.pieceDistance);
    }

    private double[][] getPieceOffsets() {
        double[][] lanePieceOffsets = new double[lanes.length][pieces.length + 1];
        for (int i = 0; i < lanePieceOffsets.length; i++) {
            for (int pi = 1; pi < lanePieceOffsets[i].length; pi++) {
                lanePieceOffsets[i][pi] = lanePieceOffsets[i][pi - 1] +
                    pieces[pi - 1].getLaneLength(lanes[i]);
            }
        }
        return lanePieceOffsets;
    }

    public int getNumLanes() {
        return lanes.length;
    }

    public int getNumPieces() {
        return pieces.length;
    }

    private int getFirstSwitchPieceIdx(TrackPiece[] trackPieces) {
        for (int i = 0; i < trackPieces.length; i++)
            if (trackPieces[i].switchLanes)
                return i;
        return -1;
    }

    private int[] trackPieceToCompMap(TrackPiece[] trackPieces) {
        int firstSwitchPieceIdx = getFirstSwitchPieceIdx(trackPieces);
        if (firstSwitchPieceIdx < 0)
            return null;

        int[] map = new int[trackPieces.length];

        int currentComp = -1;
        int i = firstSwitchPieceIdx;
        do {
            TrackPiece piece = trackPieces[i];
            if (piece.switchLanes) {
                currentComp++;
            }
            map[i] = currentComp;
            i++;
            if (i == trackPieces.length)
                i = 0;
        } while (i != firstSwitchPieceIdx);

        return map;
    }

    private List<TrackComponent> getTrackComps(int[] lanes, TrackPiece[] trackPieces) {
        int firstSwitchPieceIdx = getFirstSwitchPieceIdx(trackPieces);
        if (firstSwitchPieceIdx < 0)
            return ImmutableList.of();

        Builder<TrackComponent> trackCompsBuilder = ImmutableList.builder();

        TrackComponent comp = null;
        int i = firstSwitchPieceIdx;
        do {
            // Only add the length into the component if it isn't a switch piece
            if (trackPieces[i].switchLanes) {
                comp = new TrackComponent(lanes);
                trackCompsBuilder.add(comp);
                comp.setSwitchPiece(trackPieces[i]);
            } else {
                comp.addPiece(trackPieces[i]);
            }
            i++;
            if (i == trackPieces.length)
                i = 0;
        } while (i != firstSwitchPieceIdx);

        return trackCompsBuilder.build();
    }
}
