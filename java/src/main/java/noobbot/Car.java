/**
 * Most fields are assigned at GameInit, except for the position which is
 * modified and accessed dynamically during the race.
 **/
package noobbot;

import java.util.TreeMap;

public class Car {
    public final String name;
    public final String color;
    public final double length;
    public final double width;
    public final double guideFlagPosition;
    public final Track track;

    private final TreeMap<Integer, Position> positions;

    public Car(String name, String color, double length, double width, double guideFlagPosition, Track track) {
        this.name = name;
        this.color = color;
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
        this.positions = new TreeMap<Integer, Position>();
        this.track = track;
    }

    public void positionUpdate(Position pos) {
        positions.put(pos.gameTick, pos);
    }

    public Position getPosition() {
        return getPosition(Integer.MAX_VALUE);
    }

    public Position getPosition(int gameTick) {
        Integer key = positions.floorKey(gameTick);
        return (key == null) ? Position.NONE : positions.get(key);
    }

    public TrackPiece getPiece() {
        return getPiece(Integer.MAX_VALUE);
    }

    public TrackPiece getPiece(int gameTick) {
        return track.getPiece(getPosition(gameTick).pieceIndex);
    }

    public int getTick() {
        return getPosition().gameTick;
    }

    public double getOffset() {
        return getOffset(Integer.MAX_VALUE);
    }

    public double getOffset(int gameTick) {
        return track.getDistanceOffset(getPosition(gameTick));
    }

    public double getSpeed() {
        return getSpeed(Integer.MAX_VALUE);
    }

    public double getSpeed(int gameTick) {
        Position p1 = getPosition(gameTick);
        Position p2 = getPosition(p1.gameTick - 1);
        return track.getDistance(p2, p1) / (p1.gameTick - p2.gameTick);
    }

    public double getAngle() {
        return getAngle(Integer.MAX_VALUE);
    }

    public double getAngle(int gameTick) {
        return getPosition(gameTick).angle;
    }

    public double getAngleFirstDir() {
        return getAngleFirstDir(Integer.MAX_VALUE);
    }

    public double getAngleFirstDir(int gameTick) {
        Position p1 = getPosition(gameTick);
        Position p2 = getPosition(p1.gameTick - 1);
        return (p1.angle - p2.angle) / (p1.gameTick - p2.gameTick);
    }

    public double getAngleSecondDir() {
        return getAngleSecondDir(Integer.MAX_VALUE);
    }

    public double getAngleSecondDir(int gameTick) {
        Position p1 = getPosition(gameTick);
        Position p2 = getPosition(p1.gameTick - 1);
        return (getAngleFirstDir(p1.gameTick) - getAngleFirstDir(p2.gameTick)) /
                    (p1.gameTick - p2.gameTick);
    }

    public double getRadius() {
        return getRadius(Integer.MAX_VALUE);
    }

    public double getRadius(int gameTick) {
        TrackPiece tp = getPiece(gameTick);
        Position pos = getPosition(gameTick);
        double percentPiece = pos.pieceDistance / tp.getLaneLength(pos.laneFrom, pos.laneTo);
        return tp.radius + track.getLaneOffset(pos.laneFrom) * percentPiece +
                           track.getLaneOffset(pos.laneTo) * (1 - percentPiece);
    }

    public double getCentpForce() {
        return getCentpForce(Integer.MAX_VALUE);
    }

    public double getCentpForce(int gameTick) {
        return Math.pow(getSpeed(gameTick), 2) / getRadius(gameTick);
    }

    public static class Position {
        public static final Position NONE = new Position(0, Double.NaN, 0, Double.NaN, 0, 0, 0);
        public final int gameTick;
        public final double angle;
        public final int pieceIndex;
        public final double pieceDistance;
        public final int laneFrom;
        public final int laneTo;
        public final int lap;

        public Position(int gameTick, double angle, int pieceIndex, double pieceDistance,
                        int laneFrom, int laneTo, int lap) {
            this.gameTick = gameTick;
            this.angle = angle;
            this.pieceIndex = pieceIndex;
            this.pieceDistance = pieceDistance;
            this.laneFrom = laneFrom;
            this.laneTo = laneTo;
            this.lap = lap;
        }

        @Override
        public String toString() {
            return String.format("%6d: %+8.2f [%4d:%8.2f] %d->%d @%d", gameTick, angle,
                            pieceIndex, pieceDistance, laneFrom, laneTo, lap);
        }
    }
}
