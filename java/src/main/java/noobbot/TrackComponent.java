package noobbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrackComponent {

    private static final Logger log = LoggerFactory.getLogger(TrackComponent.class);

    private final double[] laneToLengthMap;

    private TrackPiece switchPiece = null;

    private final int[] lanes;

    public TrackComponent(int[] lanes) {
        laneToLengthMap = new double[lanes.length];
        this.lanes = lanes;
    }

    public void addPiece(TrackPiece piece) {
        if (piece.switchLanes) {
            log.error("The addPiece shouldn't have a param with switchLanes");
            return;
        }
        for (int i = 0; i < lanes.length; i++) {
            laneToLengthMap[i] += piece.getLaneLength(lanes[i]);
        }
    }

    public void setSwitchPiece(TrackPiece piece) {
        this.switchPiece = piece;
    }

    public double getLengthOfLane(int laneIndex) {
        return this.laneToLengthMap[laneIndex];
    }

    public TrackPiece getSwitchPiece() {
        return this.switchPiece;
    }

}
