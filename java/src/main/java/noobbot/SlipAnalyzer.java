package noobbot;

import java.util.concurrent.ConcurrentLinkedDeque;

import org.apache.commons.math3.optim.linear.LinearOptimizer;
import org.apache.commons.math3.optim.linear.SimplexSolver;

//TODO(anu) use http://commons.apache.org/proper/commons-math/apidocs/org/apache/commons/math3/stat/regression/OLSMultipleLinearRegression.html
//ex:http://bitingcode.blogspot.com/2012/01/simplest-olsmultiplelinearregression.html
public class SlipAnalyzer implements Runnable {

    private static final int MAX_DATA_SIZE = 100;
    private static final int REG_DATA_MAX = 10;
    private static final int REG_DATA_MIN = 5;
    private static final ConcurrentLinkedDeque<DataPoint> data = new ConcurrentLinkedDeque<DataPoint>();
    private static final SlipAnalyzer ME = new SlipAnalyzer();
    private static final LinearOptimizer o = new SimplexSolver();

    private SlipAnalyzer() {
        Thread reg = new Thread(this);
        reg.setDaemon(true);
        int priority = (int) (0.2 * (Thread.MAX_PRIORITY - Thread.MIN_PRIORITY) + Thread.MIN_PRIORITY);
        reg.setName("SlipAnalyzerBackground");
        reg.setPriority(priority);
        reg.start();
    };

    public static void addPoint(double speed, double radius, double angle, double angleChange) {
        DataPoint d = ME.new DataPoint(speed, radius, angle, angleChange);
        if (data.size() >= MAX_DATA_SIZE)
            data.removeLast();
        data.addFirst(d);
        // o.optimize(optData)
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(50);

                // incPolyFitter.clearObservations();
                // decPolyFitter.clearObservations();
                // for (ThrottleAdaptive h : data) {
                // if (moreRecentGameTick > 0) {
                // if (moreRecentGameTick == h.gameTick + 1) {
                // double speedChange = lastSpeed - h.speed;
                // double speedDiff = h.suggestedThrottle * 10 - h.speed;
                // if (Math.abs(speedChange) >= 0.000005) {
                // if (speedDiff >= 0
                // && incPolyFitter.getObservations().length < REG_DATA_MAX) {
                // incPolyFitter.addObservedPoint(speedChange, speedDiff);
                // } else if (decPolyFitter.getObservations().length <
                // REG_DATA_MAX) {
                // decPolyFitter.addObservedPoint(speedChange, speedDiff);
                // }
                // }
                // }
                // }
                // moreRecentGameTick = h.gameTick;
                // lastSpeed = h.speed;
                // }
                //
                // if (incPolyFitter.getObservations().length >= REG_DATA_MIN)
                // incPoly = new
                // PolynomialFunction(incPolyFitter.fit(polyGuess));
                // if (decPolyFitter.getObservations().length >= REG_DATA_MIN)
                // decPoly = new
                // PolynomialFunction(decPolyFitter.fit(polyGuess));
            } catch (Exception e) {
                Main.log.error(e.toString());
                e.printStackTrace();
            }
        }
    }

    private class DataPoint {
        double speed;
        double radius;
        double angle;
        double angleChange;

        public DataPoint(double speed, double radius, double angle, double angleChange) {
            this.speed = speed;
            this.radius = radius;
            this.angle = angle;
            this.angleChange = angleChange;
        }
    }
}
