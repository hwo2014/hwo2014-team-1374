package noobbot;

import com.google.gson.Gson;

abstract class SendMsg {
    abstract public void sending();

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final int gameTick;

    MsgWrapper(final String msgType, final Object data, final int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), Main.gameTick);
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }

    @Override
    public void sending() {
    }
}

class JoinRace extends SendMsg {
    public final BotId botId;
    public final String trackName;
    public final int carCount;

    JoinRace(final String name, final String key, final String trackName, final int carCount) {
        this.botId = new BotId(name, key);
        this.trackName = trackName;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }

    @Override
    public void sending() {
    }
}

class BotId {
    public final String name;
    public final String key;

    BotId(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
}

class Ping extends SendMsg {
    public static final Ping SINGLETON = new Ping();

    private Ping() {
    }

    @Override
    protected String msgType() {
        return "ping";
    }

    @Override
    public void sending() {
    }
}

class Throttle extends SendMsg {
    protected double suggestedThrottle;

    public Throttle(double value) {
        this.setValue(value);
    }

    public Throttle setValue(double t) {
        if (t >= 1.0)
            this.suggestedThrottle = 1.0;
        else if (t <= 0)
            this.suggestedThrottle = 0;
        else
            this.suggestedThrottle = t;
        return this;
    }

    public double getValue() {
        return suggestedThrottle;
    }

    @Override
    protected Object msgData() {
        return suggestedThrottle;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }

    @Override
    public void sending() {
    }
}

class SwitchLane extends SendMsg {
    public static final SwitchLane LEFT = new SwitchLane("Left");
    public static final SwitchLane RIGHT = new SwitchLane("Right");

    private final String value;

    private SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected String msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

    @Override
    public void sending() {
    }
}
