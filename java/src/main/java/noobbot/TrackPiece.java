/**
 * Most fields are assigned at GameInit, except for the position which is
 * modified and accessed dynamically during the race.
 **/
package noobbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class TrackPiece {
    private static final Logger log = LoggerFactory.getLogger(TrackPiece.class);

    public final int index;
    public final double length;
    public final double radius;
    public final double angle;
    public final boolean switchLanes;
    public final boolean isStraight;

    public TrackPiece(int index, boolean isStraight, double length, double radius, double angle,
                    boolean switchLanes) {
        this.index = index;
        this.length = length;
        this.radius = isStraight ? Double.POSITIVE_INFINITY : radius;
        this.angle = angle;
        this.switchLanes = switchLanes;
        this.isStraight = isStraight;
    }

    @Override
    public String toString() {
        return String.format("len:%.2f rad:%.2f ang:%.2f switch:%b", length, radius, angle,
                        switchLanes);
    }

    /**
     * Gets the distance from beginning to end between two lanes for this piece.
     * Note they may be the same lanes if the piece isn't a switch or switching
     * doesn't happen.
     */
    public double getLaneLength(double laneOffsetFrom, double laneOffsetTo) {
        if (laneOffsetFrom == laneOffsetTo) {
            return getLaneLength(laneOffsetFrom);
        }
        return this.getLaneSwitchLength(laneOffsetFrom, laneOffsetTo);
    }

    public double getLaneLength(double laneOffset) {
        if (isStraight) {
            return length;
        }
        return LengthUtil.computeTrackPieceLaneLength(radius, angle, laneOffset);
    }

    private double getLaneSwitchLength(double laneOffset1, double laneOffset2) {
        if (!switchLanes) {
            log.error("Should not have been calling this method");
            return Double.NaN;
        }
        if (isStraight) {
            return Math.sqrt(Math.pow(laneOffset2 - laneOffset1, 2) + length * length);
        } else {
            return getCurvedLaneSwitchLength(laneOffset1, laneOffset2);
        }
    }

    private double getCurvedLaneSwitchLength(double laneOffset1, double laneOffset2) {
        double smallerRadius = Math.min(radius - Math.signum(angle) * laneOffset1,
                        radius - Math.signum(angle) * laneOffset2);
        double largerRadius = Math.max(radius - Math.signum(angle) * laneOffset1,
                        radius - Math.signum(angle) * laneOffset2);
        double scaleFactor = largerRadius - smallerRadius;
        double scaledSmallRadius = smallerRadius / scaleFactor;
        double alpha = angle * Math.PI / 180;

        return scaleFactor
                        * (antiDerivative(scaledSmallRadius, alpha, alpha) - antiDerivative(
                                        scaledSmallRadius, alpha, 0));
    }

    private static double antiDerivative(double scaledSmallRadius, double alpha, double x) {
        double r = scaledSmallRadius + x / alpha;
        double a = Math.sqrt(Math.pow(alpha * r, 2) + 1);
        double b = x + scaledSmallRadius * alpha;
        return (a * b + Math.log(a + b)) / (2 * alpha);
    }

    /**
     * This is the pythag approx to the curved switch length, superseded by
     * {@link #getCurvedLaneSwitchLength(double, double) getCurvedLaneSwitchLength}
     *
     * They seem to perform similarly
     */
    @Deprecated
    public double getPythagoreanLaneSwitchLength(double laneFrom, double laneTo) {
        double rFrom = radius - Math.signum(angle)*laneFrom;
        double rTo = radius - Math.signum(angle)*laneTo;
        double rAvg = (rFrom+rTo)/2;
        double angleInRads = angle*Math.PI/180;

        return Math.sqrt(Math.pow(rAvg*angleInRads, 2) + Math.pow(rFrom - rTo, 2));
    }

}
