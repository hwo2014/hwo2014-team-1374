package noobbot;

public class LengthUtil {

    private LengthUtil() {
        // Utility class
    }

    private static double computeArcLength(double radius, double angle) {
        return Math.PI * 2 * radius * Math.abs(angle) / 360;
    }

    public static double computeTrackPieceLaneLength(double baseRadius, double angle, double laneOffset) {
        double radius = baseRadius - laneOffset * Math.signum(angle);
        return computeArcLength(radius, angle);
    }

}
