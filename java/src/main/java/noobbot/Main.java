package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

public class Main {

    public static final Logger log = LoggerFactory.getLogger(Main.class);

    public static int gameTick = 0;
    private String myId;
    private Car me;
    private Track track;
    private HashMap<String, Car> cars;

    // This var should be set to true every time the current component changes (if there are comps)
    private boolean componentChanged = false;
    private int curCompIndex = -1;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join)
                    throws IOException {
        // TODO: exception catch everything in this method!!!
        log.info("Started logging in Main()");
        this.writer = writer;
        String line = null;

        send(join);
        Engine.setEngine(Engine.REACTION);
        while ((line = reader.readLine()) != null) {
            long startTime = System.nanoTime();
            // =================================================================

            SendMsg msg = Ping.SINGLETON;
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            gameTick = msgFromServer.gameTick;

            if (msgFromServer.msgType.equals("carPositions")) {
                carPositions(line);

                updateCurrentComponentMetadata();

                // TODO: someone double check that this is needed, but I'm pretty sure it is
                // and not including it definitely screwed over the switch lane stuff
                if (gameTick != 0) {
                    SwitchLane switchMsg = this.getLaneChangeDecision();
                    if (switchMsg != null) {
                        msg = switchMsg;
                    } else {
                        Throttle t = getThrottle();
                        if (t != null) {
                            msg = t;
                        }
                    }
                }
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("crashed");
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                gameInit(line);
                me = cars.get(myId);
            } else if (msgFromServer.msgType.equals("yourCar")) {
                this.myId = Parsers.parseYourCar(line);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                log.warn("Unexpected msg encountered: {}", line);
            }

            send(msg);

            // =================================================================
            double milliSec = (System.nanoTime() - startTime) / 1000000.0;
            if (milliSec > Global.RESPONSE_THRESHOLD)
                log.warn(String.format("%5d : Took %.3f milliseconds", gameTick, milliSec));
        }
    }

    private void updateCurrentComponentMetadata() {
        int numComps = track.getNumComponents();
        if (numComps != 0) {
            int curCompIndex = track.getComponentIndexOfPiece(me.getPosition().pieceIndex);
            // update fields if entering a new component
            if (this.curCompIndex != curCompIndex || (numComps == 1 && me.getPiece().switchLanes)) {
                this.curCompIndex = curCompIndex;
                this.componentChanged = true;
            }
        }
    }

    double targetSpeed = 8;

    private Throttle getThrottle() {
        Throttle t = EngineAdaptive.getThrottle(gameTick, me.getSpeed(), targetSpeed);
        System.out.println(gameTick + " : " + me.getSpeed() + "\t" + t.getValue() + "\t"
                        + EngineAdaptive.nextSpeed(me.getSpeed(), t.getValue()));

        if (Math.abs(me.getSpeed() - targetSpeed) < 0.00000005) {
            System.out.printf("Reached target speed %f. Now going to %f\n", targetSpeed,
                            targetSpeed / 2.0);
            targetSpeed /= 2;
        }

        return t;
    }

    /**
     * If the next piece is a switch, this will return:
     *  - null if no lane change makes sense
     *  - a SwitchLane with the lane change desired encoded
     */
    private SwitchLane getLaneChangeDecision() {
        if (!this.componentChanged) {
            return null;
        }

        this.componentChanged = false;

        int curCompIdx = track.getComponentIndexOfPiece(me.getPiece().index);
        int nextCompIdx = (curCompIdx + 1) % track.getNumComponents();
        TrackPiece nextSwitchPiece = track.getComponent(nextCompIdx).getSwitchPiece();
        if (!nextSwitchPiece.switchLanes) {
            log.error("This should have been a switch piece!");
            return null;
        }
        if (track.getNumLanes() == 1) {
            return null;
        }
        final int curLaneIdx = me.getPosition().laneTo;
        int nextBestLaneIdx = curLaneIdx;
        double nextBestLaneLength =
                getSwitchPlusComponentLength(curLaneIdx, curLaneIdx, nextSwitchPiece);

        log.debug("If I stay in the lane: {}", nextBestLaneLength);

        if (curLaneIdx > 0) {
            double lengthIfSwitch =
                    getSwitchPlusComponentLength(curLaneIdx, curLaneIdx - 1, nextSwitchPiece);
            if (lengthIfSwitch < nextBestLaneLength) {
                nextBestLaneLength = lengthIfSwitch;
                nextBestLaneIdx = curLaneIdx - 1;
            }
            log.debug("If I switch lane: {}", lengthIfSwitch);
        }

        if (curLaneIdx + 1 < track.getNumLanes()) {
            double lengthIfSwitch =
                    getSwitchPlusComponentLength(curLaneIdx, curLaneIdx + 1, nextSwitchPiece);
            if (lengthIfSwitch < nextBestLaneLength) {
                nextBestLaneLength = lengthIfSwitch;
                nextBestLaneIdx = curLaneIdx + 1;
            }
            log.debug("If I switch lane: {}", lengthIfSwitch);
        }

        log.debug("next {} and cur {}", nextBestLaneIdx, curLaneIdx);

        if (nextBestLaneIdx == curLaneIdx) {
            return null;
        }
        return nextBestLaneIdx == curLaneIdx - 1 ?
                SwitchLane.LEFT : SwitchLane.RIGHT;
    }

    private double getSwitchPlusComponentLength(
            int laneIndexFrom,
            int laneIndexTo,
            TrackPiece switchPiece
            ) {
        if (!switchPiece.switchLanes) {
            log.error("switchPiece was not a switch piece");
        }
        double compLength = track.getComponentOfPiece(switchPiece.index)
                .getLengthOfLane(laneIndexTo);

        double laneOffsetFrom = track.getLaneOffset(laneIndexFrom);
        double laneOffsetTo = track.getLaneOffset(laneIndexTo);

        return compLength + switchPiece.getLaneLength(laneOffsetFrom, laneOffsetTo);
    }

    /* ======================== PARSING & NETWORKING ======================== */
    public static final Gson gson = new Gson();
    public static final JsonParser parser = new JsonParser();
    private final PrintWriter writer;

    private boolean gameInit(String jsonStr) {
        Parsers.GameInit gi = Parsers.GameInit.parse(jsonStr);
        System.out.println("Track id: " + gi.id());
        this.track = gi.track();
        this.cars = gi.cars(track);

        return true;
    }

    private boolean carPositions(String jsonStr) {
        Parsers.CarPositions cp = Parsers.CarPositions.parse(jsonStr);
        for (int i = 0; i < cp.size(); i++) {
            cars.get(cp.id(i)).positionUpdate(cp.position(i));
        }
        return true;
    }

    private void send(final SendMsg msg) {
        msg.sending();
        writer.println(msg.toJson());
        writer.flush();
    }

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(),
                        "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(
                        socket.getInputStream(), "utf-8"));

        // new Main(reader, writer, new JoinRace(botName, botKey, Global.USA,
        // 1));
        new Main(reader, writer, new Join(botName, botKey));
        socket.close();
    }
}
