package noobbot;

import java.util.concurrent.ConcurrentLinkedDeque;

import org.apache.commons.math3.stat.regression.SimpleRegression;

// This is my baby, nobody mess with it!
public class EngineAdaptive implements Runnable {

    private static final int MAX_HISTORY_SIZE = 100;
    private static final int REG_POINTS_MAX = 10;
    private static final int REG_POINTS_MIN = 5;
    private static final ConcurrentLinkedDeque<ThrottleAdaptive> history = new ConcurrentLinkedDeque<ThrottleAdaptive>();
    private static final EngineAdaptive SINGLETON = new EngineAdaptive();
    private static volatile SimpleRegression reg = null;
    private static volatile double leastSquaresError = Double.MAX_VALUE;

    private EngineAdaptive() {
        Thread reg = new Thread(this);
        reg.setDaemon(true);
        int priority = (int) (0.2 * (Thread.MAX_PRIORITY - Thread.MIN_PRIORITY) + Thread.MIN_PRIORITY);
        reg.setName("EngineAdaptiveBackground");
        reg.setPriority(priority);
        reg.start();
    }

    /*
     * Returns the nextSpeed given the car's current speed and throttle you set.
     * Returns NaN to indicate that we cannot estimate this.
     */
    public static double nextSpeed(double curSpeed, double curThrottle) {
        if (reg == null)
            return Double.NaN;
        double targetSpeed = curThrottle * 10;
        double throttleDiff = targetSpeed - curSpeed;
        double nextSpeed = curSpeed + (throttleDiff - reg.getIntercept()) / reg.getSlope();
        if (Double.isInfinite(nextSpeed) || Double.isNaN(nextSpeed))
            return Double.NaN;
        return nextSpeed;
    }

    public static Throttle getThrottle(int gameTick, double speed, double targetSpeed) {
        double requestedChangeInSpeed = targetSpeed - speed;
        double suggestedThrottle;

        if (reg != null) {
            suggestedThrottle = (speed + reg.predict(requestedChangeInSpeed)) / 10.0;
        } else {
            Main.log.info("Using stupid engine, because not enough data.");
            suggestedThrottle = getThrottleStupid(speed, targetSpeed);
        }

        return SINGLETON.new ThrottleAdaptive(gameTick, speed, suggestedThrottle);
    }

    private static double getThrottleStupid(double speed, double targetSpeed) {
        double percentDiff = Math.abs(targetSpeed - speed) / targetSpeed;
        if (percentDiff < 0.05) {
            double gap = targetSpeed - speed;
            double throttle = targetSpeed + gap;
            return throttle / 10.0;
        }
        if (speed < targetSpeed)
            return 1.0;
        return 0.0;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
                int moreRecentGameTick = -1;
                double lastSpeed = -1;

                SimpleRegression newReg = new SimpleRegression();

                for (ThrottleAdaptive h : history) {
                    if (moreRecentGameTick > 0) {
                        if (moreRecentGameTick == h.gameTick + 1) {
                            double speedChangeHappened = lastSpeed - h.speed;
                            double speedDiffSet = h.suggestedThrottle * 10 - h.speed;
                            if (Math.abs(speedChangeHappened) < 0.0005)
                                continue;

                            if (newReg.getN() >= REG_POINTS_MAX)
                                break;

                            newReg.addData(speedChangeHappened, speedDiffSet);
                        }
                    }
                    moreRecentGameTick = h.gameTick;
                    lastSpeed = h.speed;
                }

                if (newReg.getN() >= REG_POINTS_MIN
                                && newReg.getSumSquaredErrors() < leastSquaresError) {
                    reg = newReg;
                    leastSquaresError = newReg.getSumSquaredErrors();
                    Main.log.info("New engine model : " + strReg(newReg));
                }
            } catch (Exception e) {
                Main.log.error(e.toString());
                e.printStackTrace();
            }
        }
    }

    private static String strReg(SimpleRegression reg) {
        return String.format("y = %f x + %f [sqError = %f]", reg.getSlope(), reg.getIntercept(),
                        reg.getSumSquaredErrors());
    }

    class ThrottleAdaptive extends Throttle {
        private int gameTick;
        private double speed;

        public ThrottleAdaptive(int gameTick, double speed, double suggestedThrottle) {
            super(suggestedThrottle);
            this.gameTick = gameTick;
            this.speed = speed;
        }

        @Override
        protected Object msgData() {
            return suggestedThrottle;
        }

        @Override
        protected String msgType() {
            return "throttle";
        }

        @Override
        public void sending() {
            if (history.size() >= MAX_HISTORY_SIZE)
                history.removeLast();
            history.addFirst(this);
        }

        public String toString() {
            return gameTick + " : " + speed + " (" + suggestedThrottle + ")";
        }
    }
}
