/**
 *
 **/
package noobbot;

import java.util.HashMap;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Parsers {
    public static final JsonParser JSON_PARSER = new JsonParser();

    public static final String parseYourCar(String jsonStr) {
        JsonObject o = JSON_PARSER.parse(jsonStr).getAsJsonObject().getAsJsonObject("data");
        return o.get("name").getAsString() + o.get("color").getAsString();
    }

    public static final class GameInit {
        private final JsonObject jsonRace;
        private final JsonObject jsonTrack;

        private GameInit(String jsonStr) {
            this.jsonRace = JSON_PARSER.parse(jsonStr).getAsJsonObject().getAsJsonObject("data")
                            .getAsJsonObject("race");
            this.jsonTrack = jsonRace.getAsJsonObject("track");
        }

        public static final GameInit parse(String jsonStr) {
            return new GameInit(jsonStr);
        }

        /* "The startingPoint attribute is a visualization cue and can be ignored." */
        public String id() {
            return jsonTrack.get("id").getAsString();
        }

        public String name() {
            return jsonTrack.get("name").getAsString();
        }

        public double startX() {
            return jsonTrack.getAsJsonObject("startingPoint").getAsJsonObject("position").get("x")
                            .getAsDouble();
        }

        public double startY() {
            return jsonTrack.getAsJsonObject("startingPoint").getAsJsonObject("position").get("y")
                            .getAsDouble();
        }

        public double startAngle() {
            return jsonTrack.getAsJsonObject("startingPoint").get("angle").getAsDouble();
        }

        public Track track() {
            return new Track(pieces(), lanes());
        }

        private TrackPiece[] pieces() {
            JsonArray jsonPieces = jsonTrack.getAsJsonArray("pieces");
            TrackPiece[] pieces = new TrackPiece[jsonPieces.size()];
            for (int i = 0; i < jsonPieces.size(); i++) {
                JsonObject p = jsonPieces.get(i).getAsJsonObject();
                if (p.get("length") != null)
                    pieces[i] = new TrackPiece(i, true, p.get("length").getAsDouble(), 0, 0,
                                    p.get("switch") == null ? false : p.get("switch")
                                                    .getAsBoolean());
                else
                    pieces[i] = new TrackPiece(i, false, 0, p.get("radius").getAsDouble(), p.get(
                                    "angle").getAsDouble(), p.get("switch") == null ? false : p
                                    .get("switch").getAsBoolean());
            }
            return pieces;
        }

        private int[] lanes() {
            JsonArray jsonLanes = jsonTrack.getAsJsonArray("lanes");
            int[] lanesArr = new int[jsonLanes.size()];
            for (JsonElement lane : jsonLanes)
                lanesArr[lane.getAsJsonObject().get("index").getAsInt()] = lane.getAsJsonObject()
                                .get("distanceFromCenter").getAsInt();
            return lanesArr;
        }

        public HashMap<String, Car> cars(Track raceTrack) {
            JsonArray jsonCars = jsonRace.getAsJsonArray("cars");
            HashMap<String, Car> carsArr = new HashMap<String, Car>();
            for (JsonElement car : jsonCars) {
                JsonObject c = car.getAsJsonObject();
                String cname = c.getAsJsonObject("id").get("name").getAsString();
                String color = c.getAsJsonObject("id").get("color").getAsString();
                carsArr.put(cname + color,
                                new Car(cname, color, c.getAsJsonObject("dimensions").get("length")
                                                .getAsDouble(), c.getAsJsonObject("dimensions")
                                                .get("width").getAsDouble(), c
                                                .getAsJsonObject("dimensions")
                                                .get("guideFlagPosition").getAsDouble(), raceTrack));
            }
            return carsArr;
        }

    }

    public static final class CarPositions {
        private final JsonObject jsonCP;
        private final JsonArray jsonCars;

        private CarPositions(String jsonStr) {
            jsonCP = JSON_PARSER.parse(jsonStr).getAsJsonObject();
            jsonCars = jsonCP.getAsJsonArray("data");
        }

        public static final CarPositions parse(String jsonStr) {
            return new CarPositions(jsonStr);
        }

        public int size() {
            return jsonCars.size();
        }

        public String gameId() {
            return jsonCP.get("gameId").getAsString();
        }

        public int gameTick() {
            if (jsonCP.get("gameTick") == null)
                return 0;
            else
                return jsonCP.get("gameTick").getAsInt();
        }

        public String id(int i) {
            JsonObject jId = jsonCars.get(i).getAsJsonObject().getAsJsonObject("id");
            return jId.get("name").getAsString() + jId.get("color").getAsString();
        }

        public Car.Position position(int i) {
            JsonObject o = jsonCars.get(i).getAsJsonObject();
            return new Car.Position(gameTick(), o.get("angle").getAsDouble(), o
                            .getAsJsonObject("piecePosition").get("pieceIndex").getAsInt(), o
                            .getAsJsonObject("piecePosition").get("inPieceDistance").getAsDouble(),
                            o.getAsJsonObject("piecePosition").getAsJsonObject("lane")
                                            .get("startLaneIndex").getAsInt(),
                            o.getAsJsonObject("piecePosition").getAsJsonObject("lane")
                                            .get("endLaneIndex").getAsInt(), o
                                            .getAsJsonObject("piecePosition").get("lap").getAsInt());
        }
    }

    public static final String parseGameEnd(String jsonStr) {
        // TODO: actually parse this if there is anything we want to do with it.
        return jsonStr;
    }
}
