package noobbot;

class Global {
    public static final String GERMANY = "germany";
    public static final String FINLAND = "keimola";
    public static final String USA = "usa";

    // If we take longer than this to response to the server, we might have
    // problems. This is 25% of 1/60 of a second.
    public static final double RESPONSE_THRESHOLD = (0.25 * (1 / 60.0)) * 1000.0;
}
