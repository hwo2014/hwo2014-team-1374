package noobbot;

public class Engine {
    protected static double targetSpeed = 0.0;
    public static final EngineType REACTION = new ReactionEngine();
    public static final EngineType SIMPLE = new SimpleEngine();
    protected static EngineType engine = SIMPLE;

    public static double getThrottle(double currentSpeed, double targetSpeed) {
        return bound(engine.getThrottle(currentSpeed, targetSpeed));
    }

    public static double getThrottle(double currentSpeed) {
        return getThrottle(currentSpeed, targetSpeed);
    }

    public static void setTargetSpeed(double speed) {
        targetSpeed = speed;
    }

    public static void setEngine(EngineType type) {
        engine = type;
    }

    protected static double bound(double throttle) {
        if (throttle >= 1.0)
            return 1.0;
        if (throttle <= 0.0)
            return 0.0;
        return throttle;
    }

    private static interface EngineType {
        public double getThrottle(double currentSpeed, double targetSpeed);
    }

    private static class ReactionEngine implements EngineType {
        private static final double EPS = 0.05;

        @Override
        public double getThrottle(double currentSpeed, double targetSpeed) {
            double percentDiff = Math.abs(targetSpeed - currentSpeed) / targetSpeed;
            if (percentDiff < EPS) {
                double gap = targetSpeed - currentSpeed;
                double throttle = targetSpeed + gap;
                return throttle / 10.0;
            }
            if (currentSpeed < targetSpeed)
                return 1.0;
            return 0.0;
        }
    }

    private static class SimpleEngine implements EngineType {
        private static final double EPS = 0.05;

        @Override
        public double getThrottle(double currentSpeed, double targetSpeed) {
            double percentDiff = Math.abs(targetSpeed - currentSpeed) / targetSpeed;
            if (percentDiff < EPS)
                return bound(targetSpeed / 10.0);

            if (currentSpeed < targetSpeed)
                return 1.0;
            return 0.0;
        }
    }
}
